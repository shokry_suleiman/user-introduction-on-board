import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage  {

  constructor() {
    
  }
      // Intro method to be called 
    
  ionViewWillEnter(){
     this.introMethod();
  }

  public introMethod() {
    // import IntroJS
    const IntroJs = require("../../../node_modules/intro.js/intro");
    let intro = IntroJs();
    console.log("inside intro.js");
    intro.setOptions({
    steps: [
        {
          intro: "Ahlan wa Sahlan",
          position: 'right'
        },
        {
          element: "#step3",
          intro:
          "When you are slaves on Earth, and  you are told: ‘Renounce Earthly freedom, for in Heaven awaits you unimaginable freedom.’ Answer back: 'He who did not taste freedom on Earth, will not know it in Heaven!’",
          position: "bottom"
          
        },
        {
          element: "#step2",
          intro:
          "Most people live way too long in the past. The past is a springboard to jump forward from, not a sofa to relax on",
          position: "bottom"
        }
        ],
          showProgress: true,
          skipLabel: "Skip",
          doneLabel: "Done",
          nextLabel: "Next",
          prevLabel: "Previous",
          overlayOpacity: "0.8"
    });

    intro.start();
  }
   
}
